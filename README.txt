Compilamos primero la implementación:

-----------------
$ gcc -c slist.c
-----------------

Si todo anduvo bien, generó un slist.o en el directorio.

Luego, podemos compilar el archivo main.c, dándole
nombre "main" al binario (en lugar de "a.out"),y diciendole que tiene que tomar algunas funciones de slist.o:

----------------------------
$ gcc -o main main.c slist.o
----------------------------

Ejecutamos "main" para verificar que haga lo esperado. 

---------
$ main
---------

El juego consiste de 5 barcos para cada jugador.
Hay un barco de longuitud 4, dos barcos de longuitud 3 y dos de longuitud 2.

Los barcos estan contenidos en una lista enlazada, cada nodo representa un barco que contiene un array bidimensional de las posiciones y uno unidimensional
que contiene un 0 si la posicion no fue impactada y un 1 si si lo fue.
Los barcos se reparten aleatoriamente: Se generan 2 posiciones y una direccion, 
esta puede ser 0 si es horizontal y 1 si es vertical. Sumando uno a la posicion posterior en el array.
Se comprueba si el ultimo lugar del barco se sale del tablero, 
si es asi se repite la generacion.
Si la posicion se encuentra dentro del tablero se comprueba si colisiona con algun barco:
Se comprueba si el valor absoluto de la 
diferencia entre la coordenada x de una pieza de un barco y la coordenada x pasada es mayor a uno, si es asi devuelve 0. Se comprueba lo mismo para la coordenada y. 
Tambien se comprueba lo mismo para cada pieza de barco existente.
Si se devuelve un 0 de que hubo una colision se vuelven a generar las posiciones de manera 
aleatoria y se vuelve a comprobar.
Los disparos se guardan en una lista enlazada, cada nodo representa un disparo.