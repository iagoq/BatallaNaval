//Esto pregunta si está definida la librería, en caso de no
//estarlo la define
#ifndef __SLIST_H__
#define __SLIST_H__

#include <stddef.h>

//Esto está definiendo un nuevo tipo de dato, así como con typedef
//y struct ponemos un alias a una estructura como hemos visto en clase.
//Ahora, lo que estamos haciendo es llamar con el identificador
//FuncionVisitante a cualquier función que tome un entero y tenga tipo
//de retorno void
typedef void (*FuncionVisitante) (int dato);

typedef struct _SNodo {
  int dato, coords[2]; //Dato:0 agua, 1 impacto
  struct _SNodo *sig;
} SNodo;

typedef struct _NBarco {
   int dato[6], coords[4][2], largo; //Dato:0 no impactado, 1 si
   struct _NBarco *sig;

} NBarco;

typedef NBarco *SBarco;

//Cuando usamos SList estamos representando un SNodo*
//es decir, usar SList sería lo mismo que usar: struct _SNodo *
typedef SNodo *SList;

/**
 * Devuelve una lista vacía.
 */
SList slist_crear();

/**
 * Destruccion de la lista.
 */
void slist_destruir(SList lista);

/**
 * Determina si la lista es vacía.
 */
int slist_vacia(SList lista);

/**
 * Agrega un elemento al final de la lista.
 */
SList slist_agregar_final(SList lista, int dato);

/**
 * Agrega un elemento al inicio de la lista.
 */
SList slist_agregar_inicio(SList lista, int dato, int x, int y);


SBarco sbarco_crear();

SBarco sbarco_agregar_inicio(SBarco lista, int coords[][2], int largo);

//Genera un array bidimensional de largo*2 con las coordenadas de cada punto de un barco
void genCoords(int coords[][2],int largo);

//genera un tablero valido con 5 barcos de distintas longitudes
SBarco sbarco_generar_barcos(SBarco lista);

//devuelve true si el barco "entra" en el tablero
int chekearbarco(SBarco lista,int coords[][2],int largo);

int quedanBarcos(SBarco barcos);

int checkearImpacto(int x , int y, SBarco barcos);

int disparoValido(int x,int y,SList disparos);

SList disparar(SList disparos, SBarco barcos);

//recibe un tablero y muestra una grafica de el tablero
void mostrar_barcos(SBarco barcos);

//recibe una lista de disparos y los grafica en un tablero
void mostrar_disparos(SList disparos);

void sbarco_destruir(SBarco lista);

#endif /* __SLIST_H__ */
