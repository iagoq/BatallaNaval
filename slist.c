#include "slist.h"
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <math.h>

SList slist_crear() {
  return NULL;
}
SBarco sbarco_crear() {
  return NULL;
}

//Esta función destruye todos los nodos de la lista haciendo que
//liberemos la memoria del nodo usando free y, pasando al siguiente
//hasta llegar a NULL
void slist_destruir(SList lista) {
  SNodo *nodoAEliminar;
  while (lista != NULL) {
    nodoAEliminar = lista;
    lista = lista->sig;
    free(nodoAEliminar);
  }
}
void sbarco_destruir(SBarco lista) {
  NBarco *nodoAEliminar;
  while (lista != NULL) {
    nodoAEliminar = lista;
    lista = lista->sig;
    free(nodoAEliminar);
  }
}


SList slist_agregar_inicio(SList lista, int dato, int x, int y) {
  SNodo *nuevoNodo = malloc(sizeof(SNodo));
  nuevoNodo->dato = dato;
  nuevoNodo->coords[0] = x;
  nuevoNodo->coords[1] = y;
  nuevoNodo->sig = lista;
  return nuevoNodo;
}

int chekearbarco(SBarco lista,int actual[][2],int largo){
    //recibe un tablero de barcos y un barco extra
    //si el barco no colisiona con ningun otro barco y esta separado de otro barco por mas de una casilla devuelve true si no falso
    int i;
    for (NBarco *nodo = lista; nodo != NULL; nodo = nodo->sig){
        for(i=0; i<nodo->largo; i++){
            for(int j=0;j<largo;j++){
                if(!(abs(actual[j][0] - nodo->coords[i][0]) > 1 || abs(actual[j][1] - nodo->coords[i][1]) > 1)){
                    return 0;
                }
            }
        }
    }
    return 1;

}

SBarco sbarco_generar_barcos(SBarco lista){
    //coords cordenadas del barco, largo lista con largo de cada barco
    int i, dir;
    int largo[] = {2,2,3,3,4};
    int coords[4][2];
    for(i=0;i<5;i++){
        genCoords(coords,largo[i]);
        while(!chekearbarco(lista, coords,largo[i])){
            genCoords( coords,largo[i]);
        }
        lista=sbarco_agregar_inicio(lista,coords,largo[i]);
    }
    return lista;
}


SBarco sbarco_agregar_inicio(SBarco lista,int coords[][2],int largo) {

    NBarco *nuevoNodo = malloc(sizeof(NBarco));
    int i;

    for(i=0; i<largo;i++){
        nuevoNodo->dato[i] = 0;
        nuevoNodo->coords[i][0] = coords[i][0];
        nuevoNodo->coords[i][1] = coords[i][1];
    }
    nuevoNodo->largo = largo;
    nuevoNodo->sig = lista;
    return nuevoNodo;
}




void genCoords(int coords[][2],int largo){
    //Devuelve una lista bidimensional [largo de barco][2] con las coordenadas de de cada punto del barco
    int x,y,dir;
    coords[largo-1][0]=10;
    coords[largo-1][1]=10;
    while((coords[largo-1][0] > 9 || coords[largo-1][1] > 9)){
        x = rand()%10;
        y = rand()%10;
        dir = rand()%2;
        coords[0][0] = x;
        coords[0][1] = y;
        if(dir){
            for(int i=1;i<largo;i++){
                    coords[i][0] = x+i;
                    coords[i][1] = y;
            }
        }else{
            for(int i=1;i<largo;i++){
                    coords[i][0] = x;
                    coords[i][1] = y+i;
            }
        }
    }
}

int disparoValido(int x,int y,SList disparos){

  for(SNodo *nodo = disparos; nodo != NULL; nodo = nodo->sig){
    if(nodo->coords[0]==x && nodo->coords[1]==y){
      printf("Ya has disparado a esta casilla!\n ");
      return 0;
    }
  }

  return 1;
}
int checkearImpacto(int x , int y, SBarco barcos){
  int impactos=0;
  for (NBarco *nodo = barcos; nodo != NULL; nodo = nodo->sig){
      for(int i=0; i<nodo->largo; i++){
          if(nodo->coords[i][0]==x && nodo->coords[i][1]==y){
            nodo->dato[i]++;
            for(int y=0; y<nodo->largo; y++){
              if(nodo->dato[y]==1)
              impactos++;
            }
            if(impactos==nodo->largo){
              printf("Hundido!!!\n");
              return 1;
            }
            printf("Impacto!!!\n");
            return 1;
          }
      }
  }
  printf("Agua...\n");
  return 0;
}
int quedanBarcos(SBarco barcos){
  int impactos=0, hundidos=0;
  for (NBarco *nodo = barcos; nodo != NULL; nodo = nodo->sig){
      for(int i=0; i<nodo->largo; i++){
        if(nodo->dato[i])
        impactos++;
      }
      if(impactos == nodo->largo)
      hundidos++;
      impactos=0;
    }
    if(hundidos==5){
      return 1;
    }
    return 0;
}
SList disparar(SList disparos, SBarco barcos){
  int x=11, y=11;
  while(!disparoValido(x,y,disparos) || x==11){//checkea si se realiza un disparo a un lugar al que ya se disparo
      while(x > 10 || x < 1){//Ingreso coordenada x del disparo
        printf("\nIngrese donde quiere disparar (coordenada X): ");
        scanf("%i", &x);
        if(x>10 || x<1)
        fflush(stdin);
        printf("Error! La entrada debe estar comprendida entre 1 y 10\n");
      }
    fflush(stdin);
      while(y > 10 || y < 1){//Ingreso coordenada y del disparo
        printf("\nIngrese donde quiere disparar (coordenada Y): ");
        scanf("%i", &y);
        fflush(stdin);
        if(y>10 || y<1)
        printf("Error! La entrada debe estar comprendida entre 1 y 10\n");
      }
      x--;
      y--;
  }
    return (disparos=slist_agregar_inicio(disparos, checkearImpacto(x,y,barcos), x,y));//checkearImpacto devuelve 0 si fue agua o 1 si impacto. Se agrega el disparo a la lista de disparos
}

void mostrar_barcos(SBarco barcos){
    int tablero[10][10];
    for(int i=0; i<10; i++){
        for(int j=0; j<10; j++){
            tablero[i][j] = 176;
        }
    }
    for (NBarco *nodo = barcos; nodo != NULL; nodo = nodo->sig){
        for(int i=0; i<nodo->largo; i++){
            if(nodo->dato[i]){
                tablero[nodo->coords[i][0]][nodo->coords[i][1]] = (int)'X';
            }else{
                tablero[nodo->coords[i][0]][nodo->coords[i][1]] = (int)'O';
            }
        }
    }
    printf("  ");
    for(int i=0; i<10; i++){
        printf("%d ",i+1);
    }
    printf("\n");
    printf("  ");
    for(int i=0; i<10; i++){
        printf("- ");
    }
    printf("\n");
    for(int i=0; i<10; i++){
        printf("  ");
        for(int j=0; j<10; j++){
            printf("%c ",(char)tablero[j][i]);
        }
        printf("| %d \n",i+1);
    }
}

void mostrar_disparos(SList disparos){
    //asdasdsad
    int tablero[10][10];
    for(int i=0; i<10; i++){
        for(int j=0; j<10; j++){
            tablero[i][j] = 176;
        }
    }

    for(SNodo *nodo = disparos; nodo != NULL; nodo = nodo->sig){
        if(nodo->dato){
            int x = nodo->coords[0];
            int y = nodo->coords[1];
            tablero[x][y] = (int)'X';
        }else{
            int x = nodo->coords[0];
            int y = nodo->coords[1];
            tablero[x][y] = (int)'H';
        }
    }


    printf("  ");
    for(int i=0; i<10; i++){
        printf("%d ",i+1);
    }
    printf("\n");
    printf("  ");
    for(int i=0; i<10; i++){
        printf("- ");
    }
    printf("\n");
    for(int i=0; i<10; i++){
        printf("  ");
        for(int j=0; j<10; j++){
            printf("%c ",(char)tablero[j][i]);
        }
        printf("| %d \n",i+1);
    }
}
