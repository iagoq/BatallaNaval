#include <stdio.h>
#include <stdlib.h>
#include "slist.h"
#include <time.h>

static void imprimir_entero(int dato) {
  printf("%d ", dato);
}

int main(int argc, char *argv[]) {
  srand(time(NULL));
    int i, dato[6], coords[6][2], terminado = 0;
  char c=176, jugador[2][100];
  SBarco barcos0 = sbarco_crear();
  SList disparos0 = slist_crear();
  SBarco barcos1 = sbarco_crear();
  SList disparos1 = slist_crear();


    barcos0=sbarco_generar_barcos(barcos0);
    barcos1=sbarco_generar_barcos(barcos1);

    printf("Ingrese el nombre del jugador numero 1: ");
    scanf("%[^\n]", jugador[0]);
    fflush(stdin);
    printf("Ingrese el nombre del jugador numero 2: ");
    scanf("%[^\n]", jugador[1]);

    for(int i=0;!terminado;i++){
      system("cls");
      printf("Turno jugador %s\n", jugador[i%2]);
      if(i%2==0){
      printf("El estado de sus barcos es: \n");
      mostrar_barcos(barcos0);
      printf("Sus disparos previos: \n");
      mostrar_disparos(disparos0);
        disparos0=disparar(disparos0, barcos1);
        if(quedanBarcos(barcos1)){
          printf("Ha ganado %s", jugador[i%2]);
          terminado=1;
        }
      }else{
        printf("El estado de sus barcos es: \n");
        mostrar_barcos(barcos1);
        printf("Sus disparos previos: \n");
        mostrar_disparos(disparos1);
        disparos1=disparar(disparos1, barcos0);
        if(quedanBarcos(barcos0)){
          printf("Ha ganado %s", jugador[i%2]);
          terminado=1;
        }

      }
      system("pause");
    }

  slist_destruir(disparos0);
  slist_destruir(disparos1);
  sbarco_destruir(barcos0);
  sbarco_destruir(barcos1);
  return 0;
}
